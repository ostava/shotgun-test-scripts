#!/usr/bin/env -S bash -e

# ASAN_OPTIONS="disable_coredump=0" LSAN_OPTIONS="exitcode=0" ./xtest.sh --read ~/Projects/knot/shotgun-data/cc_60r0_20k_0_3.pcap --preload /usr/lib64/libasan.so.8 -T 8

script_dir="$(dirname $(realpath $BASH_SOURCE[0]))"
xtest_out="$script_dir/xtest-out"
xtest_charts_out="$script_dir/xtest-charts"
xtest_configs="$script_dir/xtest-configs"
tools="$HOME/Projects/knot/shotgun/main/tools"
data_dir="$HOME/Projects/knot/shotgun-data"

shotgun_args="$@"

configs=()
names=()

function run-gun {
	config="$1"
	name="$2"
	shift 2

	configs+=("$config")
	names+=("$name")

	if [ "$SHOTGUN_CONTAINER" == "1" ]; then
		export SHOTGUN_OUTPUT="$xtest_out"
		export SHOTGUN_CONFIG="$xtest_configs"
		export SHOTGUN_DATA="$data_dir"

		shotgun_out="/shotgun-out"
		shotgun_config="/shotgun-config"
	else
		shotgun_out="$xtest_out"
		shotgun_config="$xtest_configs"
	fi

	if [ "$XTEST_NO_RETEST" != "1" ]; then
		"$script_dir/shotgun.sh" \
			--config "$shotgun_config/$config" \
			--outdir "$shotgun_out/$name" \
			--force \
			"$shotgun_args"
	fi
}

rm -rf "$xtest_charts_out"
mkdir -p "$xtest_charts_out"

if [ "$XTEST_NO_RETEST" != "1" ]; then
	rm -rf "$xtest_out"
	mkdir -p "$xtest_out"
fi

#run-gun udp.toml UDP
#run-gun tcp.toml TCP

run-gun doq.toml DoQ-default
run-gun doq-no-idle.toml DoQ-no-idle
run-gun doq-1s-idle.toml DoQ-1s-idle
run-gun doq-2s-idle.toml DoQ-2s-idle
#run-gun doq-no-0rtt.toml DoQ-no-0rtt
#run-gun doq-no-tickets.toml DoQ-no-tickets
#run-gun doq-no-idle.toml DoQ-no-idle

#run-gun doh.toml DoH-default

#run-gun dot.toml DoT-default
#run-gun dot-no-idle DoT-no-idle
#run-gun dot-no-tickets DoT-no-tickets

groups=()
jsons=()
for name in "${names[@]}"; do
	json="$(echo $xtest_out/$name/data/*.json)"
	echo "json: $json"
	groups+=("-g $name $json")
	jsons+=("$json")
done

"$tools/plot-latency.py" ${groups[*]} -o "$xtest_charts_out/latency.svg"
"$tools/plot-response-rate.py" ${jsons[*]} -o "$xtest_charts_out/response-rate.svg"
"$tools/plot-connections.py" ${jsons[*]} -o "$xtest_charts_out/connections.svg" -k active

for ix in "${!jsons[@]}"; do
	"$tools/plot-connections.py" ${jsons[$ix]} -o "$xtest_charts_out/handshakes-${names[$ix]}.svg" -k conn_hs quic_0rtt tls_resumed failed_hs
	"$tools/plot-connections.py" ${jsons[$ix]} -o "$xtest_charts_out/early-${names[$ix]}.svg" -k quic_0rtt quic_0rtt_sent quic_0rtt_answered
done
