#!/bin/bash

if [ "$SHOTGUN_CONTAINER" != "1" ]; then
	shotgun_exec=(~/Projects/knot/shotgun/main/replay.py)
else
	shotgun_exec=(podman run \
		--volume "$SHOTGUN_CONFIG:/shotgun-config:ro" \
		--volume "$SHOTGUN_DATA:/shotgun-data:ro" \
		--volume "$SHOTGUN_OUTPUT:/shotgun-out" \
		--network host \
		"registry.nic.cz/knot/shotgun:quic-test" \
		replay.py)
fi

"${shotgun_exec[@]}" \
	--server ::1 \
	--dns-port 5553 \
	--dot-port 5853 \
	--doh-port 8443 \
	--doq-port 5853 \
	$@
